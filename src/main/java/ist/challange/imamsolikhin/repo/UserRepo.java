package ist.challange.imamsolikhin.repo;

import ist.challange.imamsolikhin.model.Users;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author imamsolikhin
 */
@Repository
public interface UserRepo extends JpaRepository<Users, String> {

  List<Users> findAll();
  
  Users findByUsername(String username);
  
  boolean existsByUsername(String username);

}
