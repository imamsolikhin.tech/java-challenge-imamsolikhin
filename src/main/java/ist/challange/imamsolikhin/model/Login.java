package ist.challange.imamsolikhin.model;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author imamsolikhin
 */
@Getter @Setter
public class Login {

  private String username;
  private String password;
}
