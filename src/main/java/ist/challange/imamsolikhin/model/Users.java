package ist.challange.imamsolikhin.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author imamsolikhin
 */
@Data
@Entity
@Table(name = "ist_user")
@NoArgsConstructor
public class Users implements Serializable {

  @Id
  @Column(name = "code")
  private String code;

  @NotEmpty
  @Column(name = "fullname")
  private String fullname;
  
  @NotEmpty
  @Column(name = "username")
  private String username;

  @JsonIgnore
  @Column(name = "password")
  private String password;

  @Column(name = "status")
  private boolean status = false;

}
