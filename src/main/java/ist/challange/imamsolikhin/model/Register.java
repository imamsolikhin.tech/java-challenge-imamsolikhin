package ist.challange.imamsolikhin.model;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author imamsolikhin
 */
@Getter @Setter
public class Register {

  private String fullname;
  private String username;
  private String password;
}
