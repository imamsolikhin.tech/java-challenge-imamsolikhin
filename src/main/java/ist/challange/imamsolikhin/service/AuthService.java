package ist.challange.imamsolikhin.service;

import ist.challange.imamsolikhin.model.Users;
import ist.challange.imamsolikhin.repo.UserRepo;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Service
public class AuthService implements UserDetailsService {

  @Autowired
  private UserRepo repo;

  @Override
  public UserDetails loadUserByUsername(String username) {
    Users model = repo.findByUsername(username);
    return new User(model.getUsername(), model.getPassword(),
            new ArrayList<>());
  }

  public Users loadUserDetail(String username) {
    Users sysUser = repo.findByUsername(username);
    return sysUser;
  }

  public boolean register(Users model) {
    if (repo.existsByUsername(model.getUsername())) {
      return false;
    }
    repo.saveAndFlush(model);
    return true;
  }
}
