package ist.challange.imamsolikhin.service;

import ist.challange.imamsolikhin.model.Users;
import ist.challange.imamsolikhin.repo.UserRepo;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author imamsolikhin
 */
@Service
public class UserService {

  @Autowired
  private UserRepo repo;

  public List<Users> loadAllUser() {
    return repo.findAll();

  }
}
