package ist.challange.imamsolikhin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.Authorization;
import ist.challange.imamsolikhin.service.UserService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author imamsolikhin
 */
@RestController
@RequestMapping("${jwt.path-user}/user")
@Api(tags = "User Service")
public class UserController {

  @Autowired
  private UserService service;

  @Authorization(value = "Authorization")
  @GetMapping(value = "/search")
  public ResponseEntity<?> register(HttpServletResponse response) throws IOException, Exception {
    return new ResponseEntity<>(service.loadAllUser(), HttpStatus.OK);
  }
}
