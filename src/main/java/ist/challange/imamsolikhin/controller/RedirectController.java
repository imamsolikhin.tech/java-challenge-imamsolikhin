package ist.challange.imamsolikhin.controller;

import io.swagger.annotations.Api;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author imamsolikhin
 */
@RestController
@Api(tags = "Redirect")
public class RedirectController {

  @RequestMapping("/")
  void handleBase(HttpServletResponse response) throws IOException {
    response.sendRedirect("/swagger-ui.html");
  }
  
  @RequestMapping("${jwt.path-auth}")
  void handleApi(HttpServletResponse response) throws IOException {
    response.sendRedirect("/swagger-ui.html");
  }
  

}
