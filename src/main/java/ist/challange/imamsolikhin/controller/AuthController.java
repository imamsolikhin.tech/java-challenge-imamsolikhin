package ist.challange.imamsolikhin.controller;

import io.swagger.annotations.Api;
import ist.challange.imamsolikhin.model.Login;
import ist.challange.imamsolikhin.model.Register;
import ist.challange.imamsolikhin.model.Users;
import ist.challange.imamsolikhin.security.JwtTokenUtil;
import ist.challange.imamsolikhin.service.AuthService;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author imamsolikhin
 */
@RestController
@RequestMapping("${jwt.path-auth}/auth")
@Api(tags = "Auth Service")
public class AuthController {

  @Autowired
  private AuthenticationManager authenticationManager;
  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  @Autowired
  private AuthService service;

  @PostMapping(value = "/login")
  public HttpEntity<Object> login(@RequestBody Login model, HttpServletResponse response) throws IOException, Exception {
    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
            model.getUsername(), model.getPassword()));

    final UserDetails userDetails = service.loadUserByUsername(model.getUsername());
    final String token = jwtTokenUtil.generateToken(userDetails);
    return new HttpEntity<>(token);
  }

  @PostMapping(value = "/register")
  public HttpEntity<Object> register(@RequestBody Register model, HttpServletResponse response) throws IOException, Exception {

    Users user = new Users();
    user.setCode(model.getUsername());
    user.setFullname(model.getFullname());
    user.setUsername(model.getUsername());

    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    String encodedPassword = passwordEncoder.encode(model.getPassword());
    model.setPassword(encodedPassword);
    user.setPassword(encodedPassword);
    user.setStatus(true);
    if (service.register(user)) {
      final UserDetails userDetails = service.loadUserByUsername(model.getUsername());
      final String token = jwtTokenUtil.generateToken(userDetails);
      return new HttpEntity<>(token);
    }
    return new HttpEntity<>("User has been taken");
  }
}
